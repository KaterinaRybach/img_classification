from sklearn.datasets import fetch_mldata
from sklearn.utils import shuffle
import mxnet as mx

animals = fetch_mldata('cat_dog', data_home="./cat_dog")
# shuffle data
X, y = shuffle(animals.data, animals.target)

# split dataset
train_data = X[:50000, :].astype('float32')
train_label = y[:50000]
val_data = X[50000: 60000, :].astype('float32')
val_label = y[50000:60000]

# Normalize data
train_data[:] /= 256.0
val_data[:] /= 256.0

# create a numpy iterator
batch_size = 100
train_iter = mx.io.NDArrayIter(train_data, train_label, batch_size=batch_size, shuffle=True)
val_iter = mx.io.NDArrayIter(val_data, val_label, batch_size=batch_size)

data = mx.symbol.Variable('data')
fc1 = mx.symbol.FullyConnected(data, name='fc1', num_hidden=128)
act1 = mx.symbol.Activation(fc1, name='relu1', act_type='relu')
fc2 = mx.symbol.FullyConnected(act1, name='fc2', num_hidden=64)
softmax = mx.symbol.SoftmaxOutput(fc2, name='sm')

model = mx.model.FeedForward(
         softmax,
         num_epoch=5,
         learning_rate=0.05)

# create model as usual: model = mx.model.FeedForward(...)
model.fit(X = train_iter, eval_data = val_iter)

# save a model to mymodel-symbol.json and mymodel-0100.params
prefix = 'cat_vs_dog_model'
iteration = 100
model.save(prefix, iteration)

# load model back
model_loaded = mx.model.FeedForward.load(prefix, iteration)
