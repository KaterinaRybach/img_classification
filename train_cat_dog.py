import mxnet as mx
import click
from distutils.dir_util import copy_tree
import os
import subprocess


@click.command()
@click.option('--class1', type=str, help="First class images folder.", default="cat")
@click.option('--class2', type=str, help="Second class images folder.", default="dog")
# @click.option('--modelpath', type=str, help="Model folder.")


def main(class1, class2):
    create_root_folder(class1, class2)
    subprocess.call("run.sh", shell=True)
    create_model()


def create_model():
    print("Start creating a model..")
    data_iter = mx.io.ImageRecordIter(
          path_imgrec="mydata_train.rec",
          data_shape=(3,28,28),
          batch_size=1,
          path_imglist="mydata_train.lst"
        )
    val_iter = mx.io.ImageRecordIter(
          path_imgrec="mydata_val.rec",
          data_shape=(3,28,28),
          batch_size=1,
          path_imglist="mydata_val.lst"
        )
    data = mx.symbol.Variable('data')
    fc1 = mx.symbol.FullyConnected(data=data, num_hidden=128)
    act1 = mx.symbol.Activation(data=fc1, act_type="relu")
    fc2 = mx.symbol.FullyConnected(data=act1, num_hidden=64)
    mlp = mx.symbol.SoftmaxOutput(data=fc2, name='softmax')

    prefix = "model_cat_dog"
    iterations = 100
    model = mx.model.FeedForward(
        symbol=mlp,
        num_epoch=8,
        learning_rate=0.1
    )
    model.fit(X=data_iter, eval_data=val_iter)
    model.save(prefix, iterations)


def copy_class_to_root(src, dst):
    if not os.path.isdir(dst + '//' + src):
        os.makedirs(dst + '//' + src)
    copy_tree(src, dst + '//' + src)


def create_root_folder(class1, class2):
    img_dir = "img_data"
    if not os.path.isdir(img_dir):
        os.makedirs(img_dir)
    copy_class_to_root(class1, img_dir)
    copy_class_to_root(class2, img_dir)


if __name__ == "__main__":
    main()